//statfs
#include <sys/param.h>
#include <sys/mount.h>

//xsw_usage
#include <sys/types.h>
#include <sys/sysctl.h>

//task_basic_info
#include <mach/mach.h>

//tms
#include <sys/times.h>

//getrusage
#include <sys/resource.h>
#define   RUSAGE_SELF     0
#define   RUSAGE_CHILDREN     -1

#include "Metrics.h"

using namespace std;

void totalvm() {
    struct statfs stats;
    if (0 == statfs("/", &stats)) {
        cout.precision(2);
        cout<<fixed;
        cout<<"Total Virtual Memory: "<<((double)((uint64_t)stats.f_bsize * stats.f_bfree)/(1024.0 * 1024.0))<<"M\n";
    }
}

void totalvmusage() {
    xsw_usage vmusage = {0};
    size_t size = sizeof(vmusage);
    if( sysctlbyname("vm.swapusage", &vmusage, &size, NULL, 0)!=0 ) {
        cout.precision(2);
        cout<<fixed;
        cout<<"unable to get swap usage by calling sysctlbyname(\"vm.swapusage\",...)"<<endl;
    } else {
        cout.precision(2);
        cout<<fixed;
        cout<<"Total Virtual Currently Used (sysctl -n vm.swapusage)\ntotal = "<<((double) vmusage.xsu_total) / (1024.0 * 1024.0)
        <<"M  used = "<<((double) vmusage.xsu_used) / (1024.0 * 1024.0)
        <<"M  free = "<<((double) vmusage.xsu_avail) / (1024.0 * 1024.0)
        <<"M  ";

        if (vmusage.xsu_encrypted)
          cout<<"(encrypted)\n";
        else
          cout<<"\n";
    }
}

int currentvmusage() {
    struct task_basic_info t_info;
    mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT;

    if (KERN_SUCCESS != task_info(mach_task_self(),
            TASK_BASIC_INFO, (task_info_t)&t_info, 
            &t_info_count)) {
      return -1;
    }
    else {
        cout.precision(2);
        cout<<fixed;
        // resident size is in t_info.resident_size;
        // virtual size is in t_info.virtual_size;
        cout<<"Virtual Memory Currently Used by my Process\n"
        <<"Resident size = "<<((double) t_info.resident_size) / 1024.0 
        <<"K virtual size = "<<((double) t_info.virtual_size) / (1024.0 * 1024.0)
        <<"M\n";
    }
    return 0;
}

int totalRam() {
    int mib[2];
    int64_t physical_memory;
    mib[0] = CTL_HW;
    mib[1] = HW_MEMSIZE;
    size_t length = sizeof(int64_t);

    if( sysctl(mib, 2, &physical_memory, &length, NULL, 0) != 0) {
    return -1;
    } else {
    cout.precision(2);
    cout<<fixed;
    cout<<"Total RAM available = "<<((double) physical_memory) / (1024 * 1024)<<"M\n";
    }
    return 0;
}

void memusage() {
    vm_size_t page_size;
    mach_port_t mach_port;
    mach_msg_type_number_t count;
    vm_statistics64_data_t vm_stats;

    mach_port = mach_host_self();
    count = sizeof(vm_stats) / sizeof(natural_t);
    if (KERN_SUCCESS == host_page_size(mach_port, &page_size) &&
        KERN_SUCCESS == host_statistics64(mach_port, HOST_VM_INFO, (host_info64_t)&vm_stats, &count))
    {
        long long free_memory = (int64_t)vm_stats.free_count * (int64_t)page_size;
        long long used_memory = ((int64_t)vm_stats.active_count +
                               (int64_t)vm_stats.inactive_count +
                               (int64_t)vm_stats.wire_count) *  (int64_t)page_size;
        cout.precision(2);
        cout<<fixed;
        cout<<"free memory: "
        <<free_memory / (1000 * 1000)<<"M\n"
        <<"used memory: "
        <<used_memory/ (1000*1000)<<"M\n";
    }
}

void usageinfo() {
    struct rusage r_usage;
    // struct timeval {
    //   time_t tv_sec; /* This represents the number of whole seconds of elapsed time. */
    //   long int tv_usec;  /* This is the rest of the elapsed time (a fraction of a second), */
    //                      /* represented as the number of microseconds. It is always less than one million. */
    // };

    // struct rusage {
    //              struct timeval ru_utime; /* user time used */
    //              struct timeval ru_stime; /* system time used */
    //              long ru_maxrss;          /* max resident set size */
    //              long ru_ixrss;           /* integral shared text memory size */
    //              long ru_idrss;           /* integral unshared data size */
    //              long ru_isrss;           /* integral unshared stack size */
    //              long ru_minflt;          /* page reclaims */
    //              long ru_majflt;          /* page faults */
    //              long ru_nswap;           /* swaps */
    //              long ru_inblock;         /* block input operations */
    //              long ru_oublock;         /* block output operations */
    //              long ru_msgsnd;          /* messages sent */
    //              long ru_msgrcv;          /* messages received */
    //              long ru_nsignals;        /* signals received */
    //              long ru_nvcsw;           /* voluntary context switches */
    //              long ru_nivcsw;          /* involuntary context switches */
    //      };

    getrusage(RUSAGE_SELF, &r_usage);
    fprintf(stdout, "user time used = %lf\n"\
        "system time used = %lf\n"\
        "max resident set size = %ld\n"\
        "integral shared text memory size = %ld\n"\
        "integral unshared data size = %ld\n"\
        "integral unshared stack size = %ld\n"\
        "page reclaims = %ld\n"\
        "page faults = %ld\n"\
        "swaps = %ld\n"\
        "block input operations = %ld\n"\
        "block output operations = %ld\n"\
        "messages sent = %ld\n"\
        "messages received = %ld\n"\
        "signals received = %ld\n"\
        "voluntary context switches = %ld\n"\
        "involuntary context switches = %ld\n",
        (double)r_usage.ru_utime.tv_sec + 
        (double)r_usage.ru_utime.tv_usec/1000000,
        (double)r_usage.ru_stime.tv_sec + 
        (double)r_usage.ru_stime.tv_usec/1000000,
        r_usage.ru_maxrss,
        r_usage.ru_ixrss, 
        r_usage.ru_idrss, 
        r_usage.ru_isrss,
        r_usage.ru_minflt,
        r_usage.ru_majflt,
        r_usage.ru_nswap,
        r_usage.ru_inblock,
        r_usage.ru_oublock,
        r_usage.ru_msgsnd,
        r_usage.ru_msgrcv,
        r_usage.ru_nsignals,
        r_usage.ru_nvcsw,
        r_usage.ru_nivcsw
    );
}

void diffclock(clock_t st_time, struct tms st_cpu) {
    struct tms end_cpu;
    clock_t end_time = times(&end_cpu);
    fprintf(stdout, "Real Time: %jd, User Time %jd, System Time %jd\n",
        (intmax_t)(end_time - st_time),
        (intmax_t)(end_cpu.tms_utime - st_cpu.tms_utime),
        (intmax_t)(end_cpu.tms_stime - st_cpu.tms_stime));
}

int myrandom (Complex complex) {
    double intpart, fractpart;
    fractpart = modf(complex.getReal(), &intpart);

 return std::rand()%((int)(intpart + 1));
}