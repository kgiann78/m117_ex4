#include "Complex.h"

#ifndef METRICS_H
#define METRICS_H

void totalvm();
void totalvmusage();
int currentvmusage();
int totalRam();
void memusage();
void usageinfo();
void diffclock(clock_t st_time, struct tms st_cpu);

#endif
