#include <iostream>
#include <cmath>
#include "Complex.h"
using namespace std;

Complex::Complex() {
	this->x = 0.0;
	this->y = 0.0;
	this->r = 0.0;
	this->phi = 0.0;
}

Complex::Complex(double x, double y) {
	this->x = x;
	this->y = y;
	this->r = this->getAbsolute();
	this->phi = this->getArgument();
}

Complex::Complex(Complex *complex) {
	this->x = complex->x;
	this->y = complex->y;
	this->r = complex->r;
	this->phi = complex->phi;
}

Complex::~Complex() { }

void Complex::add(Complex *complex) {
  	this->x += complex->x;
  	this->y += complex->y;
  	this->r = getAbsolute();
	this->phi = getArgument();
}

double Complex::getReal() {
    return this->x;
}
double Complex::getImagine() {
    return this->y;
}

double Complex::getAbsolute() {
    return sqrt( pow(this->x, 2) + pow(this->y, 2) );
}

double Complex::getArgument() {
	return atan2( this->y, this->x );
}

std::string Complex::getCartesian() {
    std::stringstream os;
    os.precision(2);
    os<<std::fixed;
    os<<this->x<<"+i"<<this->y;
    return os.str();
}

std::string Complex::getPolar() {
    std::stringstream os;
    os.precision(2);
    os<<std::fixed;
    os<<this->getAbsolute()<<"<"<<this->getArgument();
    return os.str();
}
