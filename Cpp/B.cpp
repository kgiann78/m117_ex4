#include <iostream>
#include <vector>
#include "Complex.h"
#include "Metrics.h"
//tms
#include <sys/times.h>

using namespace std;

int main(int argc, char* argv[])
{
    srand ( unsigned ( std::time(0) ) );
    int sizeH = 10000000;

    cout<<"Creating array"<<endl;
    Complex **complices = new Complex*[sizeH];
    for (int i = 0; i < sizeH; ++i)
    {
        complices[i] = new Complex(i, i);
    }
    Complex *complex = new Complex(0, 0);

    clock_t start = clock();
    cout<<"Start"<<endl;
    
    for (int i = 0; i < sizeH; ++i)
    {
        complex->add(complices[i]);
    }

    cout<<"Cartesian: "<<complex->getCartesian()<<endl;
    cout<<"Polar: "<<complex->getAbsolute()<<"<"<<complex->getArgument()<<endl;

    for (int i = 0; i < sizeH; ++i) {
        delete complices[i];
    }

    delete[] complices;
    delete complex;

    clock_t end = clock();
    printf ("Elapsed time %.2f ms\n", (1000*(float)(end - start))/CLOCKS_PER_SEC );
    
    currentvmusage();
    return 0;
}