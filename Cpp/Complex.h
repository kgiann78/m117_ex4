#ifndef COMPLEX_H
#define COMPLEX_H

#include <iostream>
#include <string>
#include <sstream>
#include <math.h>

class Complex
{
    public:
        Complex ();
        Complex (double x, double y);
        Complex (Complex *complex);
        virtual ~Complex();

        virtual std::string getCartesian();
        virtual std::string getPolar();
        double getReal();
        double getImagine();
        double getAbsolute();
        double getArgument();
        virtual void add(Complex *complex);

    private:
        // Cartesian form z = x + iy
        // Polar form z = r (cos(phi) + isin(phi))

        double x; // Real value 
        double y; // Imagine value
        double r; // Absolute value r = sqrt(x^2 + y^2)
        double phi; // Argument value phi = arg(z) = atan2(y, x)
};
#endif
