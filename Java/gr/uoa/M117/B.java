package gr.uoa.M117;

public class B {
    public static double getMaxMemory() {
        return Runtime.getRuntime().maxMemory() / 1e6;
    }

    public static double getTotalMemory() {
        return Runtime.getRuntime().totalMemory() / 1e6;
    }

    public static void main(String[] args) {
        System.out.println(String.format("Max memory: %.2fM\tInitial virtual memory: %.2fM", getMaxMemory(), getTotalMemory()));

        int sizeH = 10000000;

        Complex[] complices = new Complex[sizeH];
        Complex complex = new Complex(0, 0);

        System.out.println("Creating array");
        for (int i = 0; i < sizeH; i++) {
            complices[i] = new Complex(i, i);
        }

        long start = System.currentTimeMillis();
        System.out.println("Start");

        for (int i=0; i < sizeH; i++) {
            complex.add(complices[i]);
        }

        System.out.println("Cartesian: " + complex.getCartesian());
        System.out.println("Polar: " + complex.getPolar());

        long end = System.currentTimeMillis();
        System.out.println(String.format("Elapsed Time: %d ms", (end - start)));

        System.out.println(String.format("Final virtual memory used: %.2fM", getTotalMemory()));
    }
}
