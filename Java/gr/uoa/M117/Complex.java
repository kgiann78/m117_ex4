package gr.uoa.M117;

public class Complex {
    double x;
    double y;
    double r;
    double phi;

    public Complex() {
        this.x = 0;
        this.y = 0;
        this.r = 0;
        this.phi = 0;
    }

    public Complex(double x, double y) {
        this.x = x;
        this.y = y;
        this.r = getAbsolute();
        this.phi = getArgument();
    }

    public Complex(Complex complex) {
        this.x = complex.x;
        this.y = complex.y;
        this.r = getAbsolute();
        this.phi = getArgument();
    }

    public void add(Complex complex) {
        this.x += complex.x;
        this.y += complex.y;
        this.r = getAbsolute();
        this.phi = getArgument();
    }

    public double getReal() {
        return x;
    }

    public double getImagine() {
        return y;
    }

    public double getAbsolute() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    public double getArgument() {
        return Math.atan2(y, x);
    }

    public String getCartesian() {
        return String.format("%.2f+i%.2f", x, y);
    }
    public String getPolar() {
        return String.format("%.2f<%.4f", getAbsolute(), getArgument());
    }

    @Override
    public String toString() {
        return getCartesian();
    }
}
